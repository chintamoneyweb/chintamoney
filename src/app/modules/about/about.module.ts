import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutComponent } from './pages/about/about.component';
import { ContactComponent } from './pages/contact/contact.component';
import { StoryComponent } from './pages/story/story.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AboutComponent, ContactComponent, StoryComponent]
})
export class AboutModule { }

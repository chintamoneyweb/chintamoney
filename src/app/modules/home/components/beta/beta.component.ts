import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-beta',
  templateUrl: './beta.component.html',
  styleUrls: ['./beta.component.scss']
})
export class BetaComponent implements OnInit {

  headerSmallText = "We are going to launch our Private Beta soon in India. Subscribe for access!"
  mottoText = "Don't save what is left after spending; spend what is left after saving."
  mottoAuthor = "Warren Buffet"

  emailSubmitted = false;

  constructor() { }

  ngOnInit() {
  }

  submitEmail(){
    this.emailSubmitted = true;
  }

}

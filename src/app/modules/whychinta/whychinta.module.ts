import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WhyChintaComponent } from './pages/why-chinta/why-chinta.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [WhyChintaComponent]
})
export class WhychintaModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DownloadAndroidComponent } from './download-android/download-android.component';
import { FooterComponent } from './navigation/footer/footer.component';
import { TopMenuComponent } from './navigation/top-menu/top-menu.component';
import { RouterModule } from '@angular/router';
import { ModalComponent } from './modal/modal.component';
import { ModalService } from './modal/modal.service';
import { NarrowMenuComponent } from './navigation/top-menu/mobile/narrow-menu.component';
import { HamburgerComponent } from './navigation/top-menu/mobile/hamburger.component';
import { OneLineMenuComponent } from './navigation/top-menu/one-line-menu.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [DownloadAndroidComponent, FooterComponent, TopMenuComponent, ModalComponent, NarrowMenuComponent, HamburgerComponent, OneLineMenuComponent],
  exports: [
    DownloadAndroidComponent,
    FooterComponent,
    TopMenuComponent,
    ModalComponent
  ],
  providers: [
    ModalService
  ]
})
export class SharedModule { }

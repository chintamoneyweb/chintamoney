import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AboutComponent } from './modules/about/pages/about/about.component';
import { HomeComponent } from './modules/home/pages/home/home.component';
import { FaqComponent } from './modules/faq/pages/faq/faq.component';
import { ContactComponent } from './modules/about/pages/contact/contact.component';
import { PrivacyPolicyComponent } from './modules/privacy-policy/pages/privacy-policy/privacy-policy.component';
import { TermsConditionsComponent } from './modules/terms/pages/terms-conditions/terms-conditions.component';
import { TutorialModule } from './modules/tutorial/tutorial.module';
import { GuideComponent } from './modules/tutorial/pages/guide/guide.component';
import { WhyChintaComponent } from './modules/whychinta/pages/why-chinta/why-chinta.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'faq',
    component: FaqComponent
  },
  {
    path: 'contact',
    component: ContactComponent
  },
  {
    path: 'privacy',
    component: PrivacyPolicyComponent
  },
  {
    path: 'terms',
    component: TermsConditionsComponent
  },
  {
    path: 'guide',
    component: GuideComponent
  },
  {
    path: 'whychinta',
    component: WhyChintaComponent
  }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule {   }

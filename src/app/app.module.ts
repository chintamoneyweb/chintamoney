import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { RouterModule } from '@angular/router';
import { HomeModule } from './modules/home/home.module';
import { AboutModule } from './modules/about/about.module';
import { FaqModule } from './modules/faq/faq.module';
import { PrivacyPolicyModule } from './modules/privacy-policy/privacy-policy.module';
import { TutorialModule } from './modules/tutorial/tutorial.module';
import { TermsModule } from './modules/terms/terms.module';
import { WhychintaModule } from './modules/whychinta/whychinta.module';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HomeModule,
    AboutModule,
    FaqModule,
    PrivacyPolicyModule,
    TutorialModule,
    TermsModule,
    WhychintaModule,
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TermsConditionsComponent } from './pages/terms-conditions/terms-conditions.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [TermsConditionsComponent]
})
export class TermsModule { }

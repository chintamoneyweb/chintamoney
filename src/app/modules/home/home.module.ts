import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './pages/home/home.component';
import { CreateWealthComponent } from './components/create-wealth/create-wealth.component';
import { SharedModule } from '../../shared/shared.module';
import { WhyComponent } from './components/why/why.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalComponent } from '../../shared/modal/modal.component';
import { ModalService } from '../../shared/modal/modal.service';
import { BetaComponent } from './components/beta/beta.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
  ],
  declarations: [HomeComponent, CreateWealthComponent, WhyComponent, BetaComponent],
})
export class HomeModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GuideComponent } from './pages/guide/guide.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [GuideComponent]
})
export class TutorialModule { }

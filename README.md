# Chintamoney

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## 1. About

ChintaMoney is a static website which serves a purpose to introduce the ChintaMoney app & its creators to the target audience of Indian users, as well as teaches people financial literacy. The web does not contain localization, neither does it save cookies.

## 2. Project structure


---
### 1. src/styles.scss 

  Contains global definitions of styles and imports fonts.

---
### 2. src/_master.variables.scss
  Contains definition of global variables, such as font families, color palette, and font sizes. Is there mostly for easier control of the colors & fonts - you don't have to change them everywhere, only in one place.

---
### 3. assets/
  A folder that contains files to be served.
  
---
### 4. assets/fonts/
  A folder with downloaded font-families.
  
---
### 5. assets/img/
  A folder which contains images to be served.
  
---
### 6. src/app/app.component.ts
  Main component file.
  
---
### 7. src/app/app.component.html
  Template for AppComponent. Here we import top menu, footer, and router outlet.
  
---
### 7. src/app/app.component.scss
  Some unimportant css for AppComponent.
  
---
### 8. src/app/app.module.ts
  Main module for the entire app. Here we import other submodules. To learn more about what an Angular module is, click [here](https://angular.io/guide/ngmodules)

## 3. How to set the web up
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FaqComponent } from './pages/faq/faq.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [FaqComponent]
})
export class FaqModule { }

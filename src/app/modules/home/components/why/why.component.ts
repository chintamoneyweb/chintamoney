import { Component, OnInit, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { trigger, state, style,transition,animate,keyframes,query,stagger } from '@angular/animations';
import { ChangeDetectorRef } from '@angular/core';
import { ModalService } from '../../../../shared/modal/modal.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-why',
  templateUrl: './why.component.html',
  styleUrls: ['./why.component.scss','../../../../shared/modal/modal.component.scss','./why.modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('pulsate', [
      state('in', style({
        transform: 'scale(1)',
      })),
      state('out', style({
        transform: 'scale(1.2)',
      })),
      state('finale', style({
        transform: 'scale(1)'
      })),
      state('item-info', style({
        transform: 'scale(2.5)'
      })),
      transition('in => out', animate('0.8s ease-out')),
      transition('out => in', animate('0.8s ease-in')),
      transition('in => finale', animate('0.8s ease-out')),
      transition('out => finale', animate('0.8s ease-out')),
      transition('finale => item-info', animate('0.8s ease-out')),
      transition('item-info => finale', animate('0.8s ease-out'))
    ])
  ]
})
export class WhyComponent implements OnInit, AfterViewInit {

  centerAnimationTrigger = 'in';
  itemHeader = '';
  itemText = '';
  color = '';
  optionsShown = false;
  
  items = [
    {
      id:"financial-health",
      header:"Financial health",
      text:"Our ultimate goal is to make everyone financially fit and healthy.Money should be an enabler and not a roadblock to a happy life. ",
      itemUrl:"../../../../../assets/img/financial-health.svg",
      color:""
    },
    {
      id:"analytics",
      header:"Not just Analytics",
      text:"Analytics and graphs are useless unless they provoke action. ChintaMoney is a rare amalgamation of active budgeting, payments, investments and financial literacy.",
      itemUrl:"../../../../../assets/img/analytics.svg",
      color:""
    },
    {
      id:"security",
      header:"Secure",
      text:"ChintaMoney has Bank Grade security. Your data is in safe hands.",
      color: ""
    },
    {
      id:"wallet",
      header:"Not a wallet",
      text:"No need to transfer money to the app. You spend directly from your respective bank account. ",
      color:""
    },
    {
      id:"control",
      header:"Take control",
      text: "Want to reduce your Movies Budgets? Thinking of boosting your Holiday Jar? Do it on the app instantly! You Control Money, Don't Let Money Control You.",
      color: ""
    },
    {
      id:"option",
      header: "Explore!",
      color:"",
    }
  ]

  constructor(private cdr: ChangeDetectorRef, private _modal: ModalService, private router: Router) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
      this.centerAnimationTrigger = 'out';
      this.cdr.detectChanges();
  }

  toggleAnimation(){
      if(this.centerAnimationTrigger!='finale'){
        this.centerAnimationTrigger = 'finale';
      }else{
        this.centerAnimationTrigger = 'out';
      }
      this.cdr.detectChanges();
  }

  openModal(itemId: string) {
    var objectWithSettings: any;
    objectWithSettings=this.findItemSettingsIndexById(itemId);
    this.itemHeader = objectWithSettings["header"];
    this.itemText = objectWithSettings["text"];
    this.color = objectWithSettings["color"];
    if(objectWithSettings["id"]=="option"){
      this.optionsShown = true;
    }else{
      this.optionsShown = false;
    }
    this._modal.open("item-details");
  }

  closeModal(){
    this._modal.close("item-details");
  }

  findItemSettingsIndexById(itemId:string){
    for(var i = 0; i<6; i++){
      if(this.items[i]["id"] == itemId){
        return this.items[i];
      }
    }
  }

  onEnd(event) {
    if(this.centerAnimationTrigger=='finale') return;
    this.centerAnimationTrigger = 'out';
    if (event.toState === 'out') {
        this.centerAnimationTrigger = 'in';
    }
  }

  redirectTo(url: string){
    this.closeModal();
    this.router.navigate([url]);
    this.scrollTop();
  }

  scrollTop(){
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }
}


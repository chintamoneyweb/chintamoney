import { Component, OnInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { trigger, state, style,transition,animate,keyframes,query,stagger } from '@angular/animations';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-wealth',
  templateUrl: './create-wealth.component.html',
  styleUrls: ['./create-wealth.component.scss'],
  animations: [
    trigger('slideInLeft', [
      state('hide', style({
        transform: 'translateX(-100%)'
      })),
      state('show', style({
        transform: 'translateX(0)'
      })),
      transition('hide => show', [
        animate(2000, keyframes([
          style({transform: 'translateX(-100%)', offset: 0}),
          style({transform: 'translateX(15px)',  offset: 0.5}),
          style({transform: 'translateX(0)',     offset: 1.0})
        ]))
      ]),
    ]),
    trigger('slideInRight', [
      state('hide', style({
        transform: 'translateX(100%)'
      })),
      state('show',   style({
        transform: 'translateX(0)'
      })),
      transition('hide => show', [
        animate(2000, keyframes([
          style({transform: 'translateX(100%)', offset: 0}),
          style({transform: 'translateX(-15px)',  offset: 0.5}),
          style({transform: 'translateX(0)',     offset: 1.0})
        ]))
      ]),
    ])
  ]
})
export class CreateWealthComponent implements OnInit, AfterViewInit {
  
  animationTrigger = 'hide';

  paragraph1 = "'ChintaMoney Paathshaala' teaches the JamJar technique along with financial literacy using unique Audio-Visual content.";
  header1 = 'Learn';
  paragraph2 = "Create Jars and achieve your Goals of a Holiday or a Swanky Bike or be on budget with your Groceries and Utilities.";
  header2 = 'Create';
  paragraph3 = "Create rules and forget while ChintaMoney automatically segregates and saves money towards your goals.";
  header3 = 'Save';
  paragraph4 ="Put your saved money to work! ChintaMoney enables you to automatically invest in various financial products.";
  header4 = 'Invest';

  headerMain = "Don't just earn money, Create Wealth with ChintaMoney!"
  phoneUrl = "../../../../../assets/img/mockup-dashboard.svg"

  constructor(private cdr: ChangeDetectorRef, private router: Router) { }

  
  ngOnInit() {
  }

  ngAfterViewInit(){
    this.animationTrigger = 'show';
    this.cdr.detectChanges();
  }

  redirectToGuide(){
    this.router.navigate(['guide']);
    this.scrollTop();
  }

  scrollTop(){
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

}

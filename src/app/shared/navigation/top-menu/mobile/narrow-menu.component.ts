import { Component, OnInit } from '@angular/core';
import { MenuTogglerService } from '../menu-toggler.service';
import { trigger, state, style,transition,animate,keyframes,query,stagger } from '@angular/animations';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-narrow-menu',
  templateUrl: './narrow-menu.component.html',
  styleUrls: ['./narrow-menu.component.scss'],
  animations: [
    trigger('slide-menu', [
      state('closed', style({
        visibility: 'hidden',
        maxHeight: '0px',
      })),
      state('open', style({
        visibility: 'visible',
        maxHeight: '420px',
      })),
      transition('closed => open', animate('0.5s ease-out'))
    ])
  ]
})
export class NarrowMenuComponent implements OnInit {

  private menuOpened;
  private animationTrigger = 'closed';

  constructor(private _toggler: MenuTogglerService ) { }

  ngOnInit() {
    this._toggler.menuToggled.subscribe(
      (open) => {
        this.menuOpened = this._toggler.isOpen();
        this.animationTrigger = (this.menuOpened) ? 'open' : 'closed';
      }
    );
  }
}

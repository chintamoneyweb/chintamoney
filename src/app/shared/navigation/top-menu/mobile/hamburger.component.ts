import { Component, OnInit } from '@angular/core';
import { trigger, state, style,transition,animate,keyframes,query,stagger } from '@angular/animations';
import { MenuTogglerService } from '../menu-toggler.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-hamburger',
  templateUrl: './hamburger.component.html',
  styleUrls: ['./hamburger.component.scss'],
  animations: [
    trigger('cross-hb-span1', [
      state('closed', style({
        top: '0px'
      })),
      state('open', style({
        top: '15px',
        width: '0%',
        left: '50%'
      })),
      transition('closed => open', animate('0.25s ease-out')),
      transition('open => closed', animate('0.25s ease-in')),
    ]),
    trigger('cross-hb-span2', [
      state('closed', style({
        transform: 'rotate(0deg)'
      })),
      state('open', style({
        transform: 'rotate(45deg)',
      })),
      transition('closed => open', animate('0.25s ease-out')),
      transition('open => closed', animate('0.25s ease-in')),
    ]),
    trigger('cross-hb-span3', [
      state('closed', style({
        transform: 'rotate(0deg)',
      })),
      state('open', style({
        transform: 'rotate(-45deg)',
      })),
      transition('closed => open', animate('0.25s ease-out')),
      transition('open => closed', animate('0.25s ease-in')),
    ]),
    trigger('cross-hb-span4', [
      state('closed', style({
        top: '30px',
      })),
      state('open', style({
        top: '15px',
        width: '0%',
        left: '50%'
      })),
      transition('closed => open', animate('0.25s ease-out')),
      transition('open => closed', animate('0.25s ease-in')),
    ])
  ]
})
export class HamburgerComponent implements OnInit {

  hamburgerOpened;
  animationTrigger;

  constructor(private _toggler: MenuTogglerService) { }

  ngOnInit() {
    this._toggler.menuToggled.subscribe(
      (open) => { this.hamburgerOpened = this._toggler.isOpen();
                  this.animationTrigger = (this.hamburgerOpened) ? 'open' : 'closed';
                });
  }

  scrollTop(){
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  toggleMenu(){
    this._toggler.toggleMenuOpen();
    this.animationTrigger = (this.hamburgerOpened) ? 'open' : 'closed';
  }

}

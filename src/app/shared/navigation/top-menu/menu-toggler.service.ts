import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MenuTogglerService {

  private narrowMenuOpened = false;
  menuToggled:EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  toggleMenuOpen(){
     this.narrowMenuOpened = !this.narrowMenuOpened;
     this.menuToggled.emit(this.narrowMenuOpened);
  }

  isOpen(){
    return this.narrowMenuOpened;
  }
}
